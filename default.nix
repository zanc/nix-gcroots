with import <nixpkgs> { config.allowUnfree = true; };

let

  fetch = { func
          , url
          , sha256 ? "sha256:0000000000000000000000000000000000000000000000000000"
          , ...
          } @ args: builtins.${args.func} ({ name = "source";
                                             inherit url; } // builtins.removeAttrs args [ "func" ]);

  fetchurl = { ... } @ args: realise ({ url = args.url;
                                        src = fetch ({ func = "fetchurl"; } // builtins.removeAttrs args [ "name" ]);
                                      } // (if args ? name then { name = args.name; } else { }));

  fetchTarball = { ... } @ args: realise ({ url = args.url;
                                            src = fetch ({ func = "fetchTarball"; } // builtins.removeAttrs args [ "name" ]);
                                          } // (if args ? name then { name = args.name; } else { }));

  realise = { name ? builtins.baseNameOf url
            , url
            , src
            }: runCommand name {
              inherit src;
            } "ln -s $src $out";

  nixpkgs = {
    nixexprs = let
      url = "https://releases.nixos.org/nixos/24.05/nixos-24.05.2580.194846768975/nixexprs.tar.xz";
    in {
      fetchurl = fetchurl { inherit url;
                            sha256 = "sha256:1m0lwnxafpi4xsbqk7zx0ssrfsfx8w29wrmhmiysy97jfk4lpjhh"; };
      fetchTarball = fetchTarball { inherit url;
                                    sha256 = "sha256:1qzpw7s6sms6z1i8nch28ncl9lg8xk901afw5601iq5jhcaprs5p"; };
    };
    nixpkgs = let
      url = "https://api.github.com/repos/NixOS/nixpkgs/tarball/194846768975b7ad2c4988bdb82572c00222c0d7";
    in {
      fetchurl = fetchurl { name = "nixpkgs.tar.gz";
                            inherit url;
                            sha256 = "sha256:1jgdm0sv4ywnwbzdbd8cbdv95k5dj17wk6mvf2nl5d07ll37r68s"; };
      fetchTarball = fetchTarball { name = "nixpkgs.tar.gz";
                                    inherit url;
                                    sha256 = "sha256:0snj72i9dm99jlnnmk8id8ffjnfg1k81lr7aw8d01kz3hdiraqil"; };
    };
  };

  nixpkgs-unstable = {
    nixexprs = let
      url = "https://releases.nixos.org/nixos/unstable/nixos-21.11pre323695.9aeeb7574fb/nixexprs.tar.xz";
    in {
      fetchurl = fetchurl { inherit url;
                            sha256 = "sha256:1swif4kg1vkwqfbb17032kyf5v1hgc4dw3bd7p005zrx5hqjjhai"; };
      fetchTarball = fetchTarball { inherit url;
                                    sha256 = "sha256:1bw2n6pi786qcnmimvrcl666ryvm2d70ki4cljnq2hyg91yl6ipa"; };
    };
    nixpkgs = let
      url = "https://api.github.com/repos/NixOS/nixpkgs/tarball/9aeeb7574fb784eaf6395f4400705b5f619e6cc3";
    in {
      fetchurl = fetchurl { name = "nixpkgs-unstable.tar.gz";
                            inherit url;
                            sha256 = "sha256:1srn2z52nksfzhn1cqinhgh9rgchfl6shn3s1aagv9ngw0zjmcjl"; };
      fetchTarball = fetchTarball { name = "nixpkgs-unstable.tar.gz";
                                    inherit url;
                                    sha256 = "sha256:061x7m82mzlzpyxa0yc0xymdicc842dz6wii4b7mcyx96mfypy8g"; };
    };
  };

  nixos-hardware = let
    url = "https://api.github.com/repos/NixOS/nixos-hardware/tarball/46a68df837414dcdfb1c240c88d5fff1b6ef805c";
  in {
    fetchurl = fetchurl { name = "nixos-hardware.tar.gz";
                          inherit url;
                          sha256 = "sha256:0r2fwk81g2lkbmkfmsbixyl0jvkrja2zgibdfr37dic57b2nz4ck"; };
    fetchTarball = fetchTarball { name = "nixos-hardware.tar.gz";
                                  inherit url;
                                  sha256 = "sha256:1zi93w8kzsj0d2759hjbay3zrh0z62k31s56khjyixi6laxyz83a"; };
  };

  nix = {
    nix = let
      url = "https://api.github.com/repos/NixOS/nix/tarball/f15651303f8596bf34c67fc8d536b1e9e7843a87";
    in {
      fetchurl = fetchurl { name = "nix.tar.gz";
                            inherit url;
                            sha256 = "sha256:1slykafyykgqil6vmqqqy0vmsh4qzv0n8wpr184dzdvc572h7kkd"; };
      fetchTarball = fetchTarball { name = "nix.tar.gz";
                                    inherit url;
                                    sha256 = "sha256:06nicg11ldbh42zp92xywa1ldjxifazn9ks3zggwlx6bh1kkg8qy"; };
    };
    nixpkgs = let
      url = "https://api.github.com/repos/NixOS/nixpkgs/tarball/70717a337f7ae4e486ba71a500367cad697e5f09";
    in {
      fetchurl = fetchurl { name = "nixpkgs.tar.gz";
                            inherit url;
                            sha256 = "sha256:0v9ya8cvk0ncvahwaq5sym3sz2bgi7ab74n1qyglypy5n8bjhv86"; };
      fetchTarball = fetchTarball { name = "nixpkgs.tar.gz";
                                    inherit url;
                                    sha256 = "sha256:1sbmqn7yc5iilqnvy9nvhsa9bx6spfq1kndvvis9031723iyymd1"; };
    };
  };

  templates = let
    url = "https://api.github.com/repos/NixOS/templates/tarball/98414b0eea82c92417fdefcec843e574c61a8cd3";
  in {
    fetchurl = fetchurl { name = "templates.tar.gz";
                          inherit url;
                          sha256 = "sha256:0h0fkjfwyia6qs2azf0p7sdn5yvdh60j3pf70715qpkmbwy9qma9"; };
    fetchTarball = fetchTarball { name = "templates.tar.gz";
                                  inherit url;
                                  sha256 = "sha256:19chca8mvn00zm0dfa5hlxdqijny4c3iwj50caggrdpbbr6r4raz"; };
  };

  nixus = {
    nixus = let
      url = "https://api.github.com/repos/Infinisil/nixus/tarball/2127516f68ce6f900b38289985377559e69aab88";
    in {
      fetchurl = fetchurl { name = "nixus.tar.gz";
                            inherit url;
                            sha256 = "sha256:07ilfdsngf2lcd42ql148mdyg6vi4b4rshwqfk1nlmxv3ply0ch5"; };
      fetchTarball = fetchTarball { name = "nixus.tar.gz";
                                    inherit url;
                                    sha256 = "sha256:1y709s5h3mvf7ksr0ibk2kcrkmsvpzzgvmgdjvni249sh4vbiw33"; };
    };
    nixpkgs = let
      url = "https://github.com/NixOS/nixpkgs/tarball/3320a06049fc259e87a2bd98f4cd42f15f746b96";
    in {
      fetchurl = fetchurl {
        name = "nixpkgs.tar.gz";
        inherit url;
        sha256 = "sha256:16j4m45ic86drabpir7n7izwabbx740pgj729i165za1l2b8d9h5";
      };
      fetchTarball = fetchTarball {
        name = "nixpkgs.tar.gz";
        inherit url;
        sha256 = "sha256:1g5l186d5xh187vdcpfsz1ff8s749949c1pclvzfkylpar09ldkl";
      };
    };
  };

in [

  nixpkgs.nixexprs.fetchurl
  nixpkgs.nixexprs.fetchTarball
  nixpkgs.nixpkgs.fetchurl
  nixpkgs.nixpkgs.fetchTarball

  nixpkgs-unstable.nixexprs.fetchurl
  nixpkgs-unstable.nixexprs.fetchTarball
  nixpkgs-unstable.nixpkgs.fetchurl
  nixpkgs-unstable.nixpkgs.fetchTarball

  nixos-hardware.fetchurl
  nixos-hardware.fetchTarball

  nix.nix.fetchurl
  nix.nix.fetchTarball
  nix.nixpkgs.fetchurl
  nix.nixpkgs.fetchTarball

  templates.fetchurl
  templates.fetchTarball

  nixus.nixus.fetchurl
  nixus.nixus.fetchTarball
  nixus.nixpkgs.fetchurl
  nixus.nixpkgs.fetchTarball

  #steam-run
  #scanmem
  #zsnes
  #mednafen

]
